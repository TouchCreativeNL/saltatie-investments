# Saltatie Investments

## Om te beginnen

Om te beginnen met dit project, volg de onderstaande stappen:

**1. Clone de repository of unzip de source code**

**2. Installeer de dependencies**

Zorg dat Node.js v16.17.1 en npm geïnstalleerd zijn op jouw machine. Daarna run:

```
npm install
```

**3. Run de development server**

```
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) in de browser om het resultaat te bekijken.

## Over het project

Het project is gebouwd in Nuxt2 en gebruikt Static Site Generation voor productie. Na wijzigingen in het project, gebruik onderstaande command om te genereren:

```
npm run generate
```

Via FTP kunnen dan de nieuw gegenereerde bestanden op de server gezet worden, deze staan in de `dist`-map. Bestanden die altijd opnieuw op de server gezet dienen te worden zijn:

- 200.html
- index.html
- .nuxt (map)

Daarnaast alleen de mappen/bestanden vervangen die gewijzigd zijn.

> Op deze server hebben wij geen toegang tot alle mappen. De te wijzigen bestanden moeten daarom in het `touch`-mapje op de server gezet worden. Daarna Gijsbert vragen naar Helgo. Hij zet het dan op de juiste plek op de server.

## BELANGRIJK!

Wanneer het project geupload moet worden op de server, moet na `npm run generate` de LinkedIn tag nog toegevoegd worden.
Navigeer naar de `/dist`-map en bewerk de index.html.

Voeg direct na de `<body>` onderstaand script toe:

 <script type="text/javascript"> _linkedin_partner_id = "4345993"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(l) { if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])}; window.lintrk.q=[]} var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(window.lintrk); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=4345993&fmt=gif" /> </noscript>
