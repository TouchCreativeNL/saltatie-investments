import en from '../locale/en.json'
import nl from '../locale/nl.json'
export default {
    locale: 'nl',
    fallbackLocale: 'nl',
    messages: { en, nl }
}
