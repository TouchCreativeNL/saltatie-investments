import i18n from "./config/i18n";

export default {
  mode: "spa",
  target: "static" /* or 'server' */,
  router: {
    base: "/",
  },

  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  /*
   ** Headers of the page
   */
  head: {
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || "",
      },
    ],
    link: [
      {
        rel: "icon",
        type: "image/x-icon",
        href: "/images/favicon.ico",
      },
    ],
    script: [
      {
        type: "text/javascript",
        src: "https://consent.cookiebot.com/uc.js",
        id: "Cookiebot",
        "data-cbid": "35a238e5-8085-43ed-acd3-661685098e15",
        body: true,
      },
      {
        type: "text/javascript",
        src: "/js/scrollreveal.min.js",
        body: true,
        async: true,
        crossorigin: "anonymous",
        defer: true,
      },
      {
        type: "text/javascript",
        src: "/js/swiper-bundle.min.js",
        body: true,
        async: true,
        crossorigin: "anonymous",
        defer: true,
      },
      {
        type: "text/javascript",
        src: "/js/main.js",
        body: true,
        async: true,
        crossorigin: "anonymous",
        defer: true,
      },
      {
        src: "https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js",
        type: "text/javascript",
        body: true,
        async: true,
        crossorigin: "anonymous",
        defer: true,
      },
      {
        type: "text/javascript",
        src: "/js/video.js",
        body: true,
        async: true,
        crossorigin: "anonymous",
        defer: true,
      },
      {
        type: "text/javascript",
        src: "https://maps.googleapis.com/maps/api/js?key=AIzaSyB06tOJATxkHR4o6cmiCkKEzHJnzfflIrA&callback=initMap&libraries=&v=weekly",
        body: true,
        async: true,
      },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },
  /*
   ** Global CSS
   */
  css: [
    // SCSS file in the project
    "~/sass/main.scss",
    "~assets/style/swiper-bundle.min.css",
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ["@/plugins/youtube"],

  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    "@nuxtjs/fontawesome",
    [
      "@nuxt/image",
      {
        target: "static",
      },
    ],
    [
      "nuxt-i18n",
      {
        vueI18nLoader: true,
        defaultLocale: "nl",
        locales: [
          {
            code: "en",
            name: "English",
          },
          {
            code: "nl",
            name: "Dutch",
          },
        ],
        vueI18n: i18n,
      },
    ],
  ],

  fontawesome: {
    icons: {
      solid: true,
      brands: true,
    },
  },

  /*
   ** Nuxt.js modules
   */
  modules: [
    [
      "nuxt-gmaps",
      {
        key: "AIzaSyB06tOJATxkHR4o6cmiCkKEzHJnzfflIrA",
        //you can use libraries: ['places']
      },
    ],
    [
      "@nuxt/image",
      {
        target: "static",
      },
    ],
    "@nuxtjs/i18n",
  ],

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
  },
};
